# -*- coding: utf-8 -*-

import struct

from s2_gen5.driver import defs
from s2_gen5.driver.communication import S2Payload

__author__ = 'gregory'
__copyright__ = "Copyright 2018, Alpes Lasers SA"


class S2Info(S2Payload):
    __slots__ = ('device_id', 'sw_version', 'hw_version', 'input_voltage_measured', 'output_voltage_measured',
                 'output_current_measured', 'MCU_temperature', 'laser_temperature',
                 'output_current_measured_out_of_pulse', 'status', 'pulse_clock_frequency', 'API_version',
                 'aux_input_measured')
    _STRUCT = struct.Struct('<IHHffffffHIIf')


class S2Settings(S2Payload):
    __slots__ = ('pulse_period', 'pulse_width', 'sync_out_width', 'output_voltage_set', 'output_current_limit',
                 'pulsing_mode', 'bias_t', 'burst_ON', 'burst_OFF', 'aux_output')
    _STRUCT = struct.Struct('<IIIffHfIIf')

    @classmethod
    def default(cls):
        """

        :return S2Settings:
        """
        return S2Settings(pulse_period=500, pulse_width=1, sync_out_width=200, output_voltage_set=1.0, aux_output=0,
                          output_current_limit=0.1, pulsing_mode=defs.S2_PULSING_OFF, bias_t=0, burst_ON=0, burst_OFF=0)


class S2Uptime(S2Payload):
    __slots__ = 'uptime', 'total_uptime'
    _STRUCT = struct.Struct('<QQ')


class S2ResetStatus(S2Payload):
    __slots__ = 'status_flag',
    _STRUCT = struct.Struct('<H')


_packet_type_to_payload = {defs.S2_PACKET_INFO: S2Info,
                           defs.S2_PACKET_SET_SETTINGS: S2Settings,
                           defs.S2_PACKET_SET_PERSISTENT_SETTINGS: S2Settings,
                           defs.S2_PACKET_QUERY_SETTINGS: S2Settings,
                           defs.S2_PACKET_UPTIME: S2Uptime,
                           defs.S2_PACKET_RESET_STATUS_FLAG: S2ResetStatus, }
