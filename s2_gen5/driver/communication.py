# -*- coding: utf-8 -*-
"""
Created by gregory on 14.02.18

Copyright 2018 Alpes Lasers SA, Neuchatel, Switzerland
"""
import logging
import struct
import threading
import time

import serial

logger = logging.getLogger(__name__)

__author__ = 'gregory'
__copyright__ = "Copyright 2018, Alpes Lasers SA"

S2_PACKET_TYPE_SIZE = 2
S2_PACKET_DATA_SIZE = 60
S2_CHECKSUM_SIZE = 2
S2_PACKET_SIZE = S2_PACKET_TYPE_SIZE + S2_PACKET_DATA_SIZE + S2_CHECKSUM_SIZE

END = b'\xc0'
ESC = b'\xdb'
ESC_END = b'\xdb\xdc'
ESC_ESC = b'\xdb\xdd'


_S2PacketTypeStruct = struct.Struct('<H')


class S2Payload(object):
    __slots__ = ()
    _STRUCT = struct.Struct('')

    def __init__(self, *args, **kwargs):
        if args:
            if len(args) != len(self.__slots__):
                raise ValueError('Provide {} positional arguments'.format(len(self.__slots__)))
            for k, v in zip(self.__slots__, args):
                setattr(self, k, v)
        if kwargs:
            for k, v in kwargs.items():
                setattr(self, k, v)

    def pack_into(self, buffer):
        self._STRUCT.pack_into(buffer, S2_PACKET_TYPE_SIZE, *self)

    def unpack_from(self, buffer):
        values = self._STRUCT.unpack_from(buffer, S2_PACKET_TYPE_SIZE)
        for k, v in zip(self.__slots__, values):
            setattr(self, k, v)

    def to_dict(self):
        return {k: getattr(self, k) for k in self.__slots__}

    def __iter__(self):
        return (getattr(self, x) for x in self.__slots__)

    def __repr__(self):
        return '{}: {}'.format(type(self).__name__, ', '.join(map(lambda k: '{}={}'.format(k, getattr(self, k)),
                                                                  self.__slots__)))


def _fletcher16(data):
    sum_1 = 0
    sum_2 = 0
    for b in data:
        sum_1 = (sum_1 + b) % 255
        sum_2 = (sum_1 + sum_2) % 255
    return bytearray([sum_1, sum_2])


def _slip_encode(data):
    return END + data.replace(ESC, ESC_ESC).replace(END, ESC_END) + END


def _slip_decode(data):
    return data.strip(END).replace(ESC_END, END).replace(ESC_ESC, ESC)


def _packet_wrap(data):
    if len(data) != S2_PACKET_SIZE:
        raise ValueError('data is not a valid S2 packet')
    chksum = _fletcher16(data[:-S2_CHECKSUM_SIZE])
    data[-S2_CHECKSUM_SIZE:] = chksum
    return _slip_encode(data)


def _packet_unwrap(packet):
    data = _slip_decode(packet)
    if len(data) != S2_PACKET_SIZE:
        raise ValueError('data is not a valid S2 packet\n{}'.format(data))
    chksum = _fletcher16(data[:-S2_CHECKSUM_SIZE])
    if data[-S2_CHECKSUM_SIZE:] != chksum:
        logger.debug(data)
        logger.debug(chksum)
        raise ValueError('Invalid checksum')
    return data


def create_packet(packet_type, payload=None):
    data = bytearray(S2_PACKET_SIZE)
    _S2PacketTypeStruct.pack_into(data, 0, packet_type)
    if payload is not None:
        payload.pack_into(data)
    return _packet_wrap(data)


def _parse_packet(packet, payload=None):
    data = _packet_unwrap(packet)
    if payload is not None:
        payload.unpack_from(data)


class S2Base(object):

    def __init__(self, th):
        self.th = th
        self.max_voltage_step = 0.4  # Voltage step used for the ramping
        self.voltage_ramp_speed = 2.0  # voltage speed in [V/s] used for the ramping
        self._lock = threading.RLock()
        self._num_commands_sent = 0
        self._num_commands_failed = 0
        self._ignore_comm_count = False

    def _read_packet(self, payload=None):
        """Reads a packet from the transport handler. Discards empty messages as they probably result from the first END
        delimiter of SLIP packets"""
        while True:
            msg = self.th.read()
            if len(msg) < S2_PACKET_SIZE:
                continue
            try:
                _parse_packet(msg, payload)
                return payload
            except Exception as e:
                logger.debug('Could not parse message: "{}"'.format(msg))
                raise e

    def _query_packet(self, packet, response_payload=None):
        """Method to send a packet to the S2 with a retry if necessary. The retry policy is to try for 3 secs max but
        at least twice"""
        with self._lock:
            start = time.time()
            num_tries = 0
            while True:
                num_tries += 1
                try:
                    if not self._ignore_comm_count:
                        self._num_commands_sent += 1
                    self.th.write(packet)
                    return self._read_packet(response_payload)
                except Exception as e:
                    logger.debug('S2 communication error [{}]'.format(e))
                    if not self._ignore_comm_count:
                        self._num_commands_failed += 1
                    if num_tries >= 2 and time.time() - start >= 3:
                        raise
                    time.sleep(0.1)


class S2SerialHandler(object):
    def __init__(self, port):
        self._serial = serial.Serial(baudrate=38400, timeout=0.2)
        self._serial.port = port

    def set_port(self, port):
        if self.is_open():
            raise Exception('Cannot change port while connected')
        self._serial.port = port

    def open(self):
        self._serial.open()
        self._serial.flushInput()
        self._serial.flushOutput()

    def close(self):
        self._serial.close()

    def is_open(self):
        return self._serial.is_open

    def write(self, data):
        self._serial.write(data)

    def read(self):
        msg = bytearray()
        while True:
            c = self._serial.read(1)
            if not c:
                raise Exception('Timeout')
            if c == END:
                if msg:
                    return msg
            else:
                msg += c