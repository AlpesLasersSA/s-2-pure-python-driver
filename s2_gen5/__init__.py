# -*- coding: utf-8 -*-

import logging

logger = logging.getLogger(__name__)

__author__ = 'gregory'
__copyright__ = "Copyright 2018, Alpes Lasers SA"


from s2_gen5.driver.defs import (S2_PULSING_OFF, S2_PULSING_INTERNAL, S2_PULSING_EXTERNAL, S2_PULSING_EXTERNAL_TRIGGER,
                                 S2_PULSING_BURST, S2_PULSING_BURST_EXTERNAL_TRIGGER, S2_PULSING_FULL_EXTERNAL)

from s2_gen5.driver.exceptions import S2InvalidPulseParamsError

from s2_gen5.driver.driver import S2
