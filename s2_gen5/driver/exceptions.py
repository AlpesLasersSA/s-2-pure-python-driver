# -*- coding: utf-8 -*-


__author__ = 'gregory'
__copyright__ = "Copyright 2018, Alpes Lasers SA"


class S2InvalidPulseParamsError(Exception):
    pass
