# -*- coding: utf-8 -*-

import s2_gen5


__author__ = 'gregory'
__copyright__ = "Copyright 2018, Alpes Lasers SA"


if __name__ == '__main__':
    s2 = s2_gen5.S2('/dev/ttyUSB1')
    s2.open()
    try:
        print('Successfully connected to S-2 #{}'.format(s2.device_id))
        print('Switching on pulsing: 2V, 10000ns period, 500ns pulse width')

        s2.set_settings(s2_gen5.S2_PULSING_INTERNAL, voltage=2, pulse_period=10000, pulse_width=500)

        print('You must call reload_info() to fetch the latest measurements from the S-2')

        s2.reload_info()

        print('Currently measured current: {:.2f}A; measured voltage: {:.2f}V'.format(s2.measured_current,
                                                                                      s2.measured_voltage))
        print('Switching off the S-2')

        s2.set_settings(s2_gen5.S2_PULSING_OFF)

    finally:
        s2.close()