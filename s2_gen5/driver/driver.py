# -*- coding: utf-8 -*-
"""
Created by gregory on 14.02.18

Copyright 2018 Alpes Lasers SA, Neuchatel, Switzerland
"""
import logging
import threading
import time

from s2_gen5.driver.communication import create_packet, S2Base, S2SerialHandler
from s2_gen5.driver.exceptions import S2InvalidPulseParamsError

from s2_gen5.driver import defs
from s2_gen5.driver import payloads

logger = logging.getLogger(__name__)

__author__ = 'gregory'
__copyright__ = "Copyright 2018, Alpes Lasers SA"


_info_query_packet = create_packet(defs.S2_PACKET_INFO)
_settings_query_packet = create_packet(defs.S2_PACKET_QUERY_SETTINGS)
_uptime_query_packet = create_packet(defs.S2_PACKET_UPTIME)
_reset_overcurrent_packet = create_packet(defs.S2_PACKET_RESET_STATUS_FLAG,
                                          payloads.S2ResetStatus(defs.S2_STATUS_OVERCURRENT))

_available_pulsing_modes = {defs.S2_PULSING_OFF,
                            defs.S2_PULSING_INTERNAL,
                            defs.S2_PULSING_BURST,
                            defs.S2_PULSING_BURST_EXTERNAL_TRIGGER,
                            defs.S2_PULSING_EXTERNAL_TRIGGER,
                            defs.S2_PULSING_EXTERNAL}

class S2(S2Base):
    """
    Driver to control an S-2 connected to a serial port.
    """

    STATUS_LABELS = {defs.S2_STATUS_OK: 'ok',
                     defs.S2_STATUS_OVERCURRENT: 'overcurrent',
                     defs.S2_STATUS_UNDERVOLTAGE: 'undervoltage'}

    @property
    def device_id(self):
        return self._info.device_id

    @property
    def sw_version(self):
        return self._info.sw_version

    @property
    def hw_version(self):
        return self._info.hw_version

    @property
    def pulsing_mode(self):
        return self._settings.pulsing_mode
    @property
    def status_label(self):
        return self.STATUS_LABELS.get(self._info.status, 'unknown status')

    @property
    def is_overcurrent(self):
        return self._info.status == defs.S2_STATUS_OVERCURRENT

    @property
    def is_undervoltage(self):
        return self._info.status == defs.S2_STATUS_UNDERVOLTAGE

    @property
    def measured_current(self):
        return self._info.output_current_measured

    @property
    def measured_voltage(self):
        return self._info.output_voltage_measured

    @property
    def pulse_width_min(self):
        return self._pulse_width_min

    @property
    def pulse_width_max(self):
        return self._pulse_width_max

    @property
    def pulse_period_min(self):
        return self._pulse_period_min

    @property
    def pulse_period_max(self):
        return self._pulse_period_max

    @property
    def step_ns(self):
        return 1e9 / self._info.pulse_clock_frequency

    @property
    def voltage_min(self):
        return self._min_voltage

    @property
    def voltage_max(self):
        return self._max_voltage

    @property
    def pulse_period(self):
        return self._settings.pulse_period * self.step_ns

    @property
    def pulse_width(self):
        return self._settings.pulse_width * self.step_ns

    @property
    def duty_cycle(self):
        return float(self._settings.pulse_width) / float(self._settings.pulse_period)

    @property
    def applied_voltage(self):
        return self._settings.output_voltage_set

    @property
    def current_limit(self):
        return self._settings.output_current_limit

    @property
    def last_info_time(self):
        return self._last_info_time

    @property
    def set_settings_time(self):
        return self._set_settings_time

    def __init__(self, serial_port):
        th = S2SerialHandler(serial_port)
        super(S2, self).__init__(th)

        self._settings = payloads.S2Settings.default()
        self._info = payloads.S2Info()
        self._uptime = payloads.S2Uptime()
        self._lock = threading.RLock()
        self._last_info_time = 0
        self._set_settings_time = 0
        self._min_voltage = 0
        self._max_voltage = 25
        self._pulse_period_min = 500
        self._pulse_period_max = 42949672950
        self._pulse_width_min = 10
        self._pulse_width_max = 42949672950
        self._sync_out_min = 10
        self._sync_out_max = 42949672950
        self._max_API_version = 2017102401

    def open(self):
        """Open the serial port. Fetch the S-2 information and check the compatibility with the current driver."""
        with self._lock:
            self.th.open()
            self.reload_info()
            if self.step_ns != 10:
                raise Exception('Step size sanity check failed (reports: {} instead of {})'.format(self.step_ns, 10))

            if self._info.hw_version != 5:
                raise Exception('S-2 hardware version {} not supported by this '
                                'gen 5 driver'.format(self._info.hw_version))
            api_version = self._info.API_version
            if api_version > self._max_API_version:
                raise Exception('S-2 firmware too recent ({}). Please update this program [S-2 #{}]'.format(
                    self._info.API_version, self._info.device_id))
            if api_version < 2017102401:
                raise Exception('S-2 firmware too old ({}). Please update the S-2 #{}'.format(
                    self._info.API_version, self._info.device_id))
            self._query_packet(_settings_query_packet, self._settings)

    def close(self):
        """Close the serial port"""
        with self._lock:
            self.th.close()

    def reload_info(self):
        """Fetch the information from the S-2

        You must call this to refresh the measurement results"""
        self._query_packet(_info_query_packet, self._info)
        self._last_info_time = time.time()

    def reset_overcurrent_flag(self):
        """Reset the overcurrent critical condition"""
        self._query_packet(_reset_overcurrent_packet)

    def set_settings(self, pulsing_mode=None, voltage=None, pulse_period=None, pulse_width=None, current_limit=None,
                     burst_ON=None, burst_OFF=None, sync_out_width=None):
        """Set the specified settings to the S-2. The unspecified parameters (=None) are not changed.

        :param pulsing_mode: S-2 pulsing mode [integer]
        :param voltage: applied voltage [V]
        :param pulse_period: [ns]
        :param pulse_width: [ns]
        :param current_limit: [A]
        :param burst_ON: number of pulsing periods ON, must be a multiple of 10
        :param burst_OFF: number of pulsing periods OFF, must be a multiple of 10
        :param sync_out_width: [ns]
        """
        with self._lock:
            if pulsing_mode is not None:
                if pulsing_mode not in _available_pulsing_modes:
                    raise S2InvalidPulseParamsError('Unknown pulsing mode "{}"'.format(pulsing_mode))
                self._settings.pulsing_mode = pulsing_mode

            if voltage is not None:
                if not self._min_voltage <= voltage <= self._max_voltage:
                    raise S2InvalidPulseParamsError('Voltage {} out of bounds ({}, {})'.format(voltage,
                                                                                               self._min_voltage,
                                                                                               self._max_voltage))
                self._settings.output_voltage_set = voltage

            if pulse_period is not None:
                if not self._pulse_period_min <= pulse_period <= self._pulse_period_max:
                    raise S2InvalidPulseParamsError('Pulse period {} out of bounds ({}, {})'.
                                                    format(pulse_period,
                                                           self._pulse_period_min, self._pulse_period_max))
                self._settings.pulse_period = int(1e-9 * pulse_period * self._info.pulse_clock_frequency)

            if pulse_width is not None:
                if not self._pulse_width_min <= pulse_width <= self._pulse_width_max:
                    raise S2InvalidPulseParamsError('Pulse width {} out of bounds ({}, {})'.
                                                    format(pulse_width, self._pulse_width_min, self._pulse_width_max))
                self._settings.pulse_width = int(1e-9 * pulse_width * self._info.pulse_clock_frequency)

            if current_limit is not None:
                self._settings.output_current_limit = current_limit

            if burst_ON is not None:
                if burst_ON % 10 != 0:
                    raise S2InvalidPulseParamsError('Burst ON periods must be a multiple of 10')
                self._settings.burst_ON = burst_ON

            if burst_OFF is not None:
                if burst_OFF % 10 != 0:
                    raise S2InvalidPulseParamsError('Burst OFF periods must be a multiple of 10')
                self._settings.burst_OFF = burst_OFF

            if sync_out_width is not None:
                if not self._sync_out_min <= sync_out_width <= self._sync_out_max:
                    raise S2InvalidPulseParamsError('Sync out width {} out of bounds ({}, {})'.
                                                    format(sync_out_width, self._sync_out_min, self._sync_out_max))

            logger.debug('Applying {}'.format(self._settings))
            packet = create_packet(defs.S2_PACKET_SET_SETTINGS, self._settings)
            self._query_packet(packet, self._settings)
            self._set_settings_time = time.time()
            time.sleep(0.2)

    def get_uptime(self):
        """Fetch the uptime information from the S-2 and return the uptime payload"""
        return self._query_packet(_uptime_query_packet, self._uptime)
